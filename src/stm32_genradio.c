/****************************************************************************
 * configs/nucleo-l476rg/src/stm32_genradio.c
 *
 *   Copyright (C) 2014 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>

#include <stdint.h>
#include <stdbool.h>
#include <debug.h>
#include <errno.h>

#include <nuttx/spi/spi.h>
#include <arch/board/board.h>
#include <nuttx/wireless/generic/cc1101.h>
#include <nuttx/wireless/generic/si4463.h>
#include <nuttx/wireless/generic/sx1231.h>
#include <nuttx/wireless/generic/genradio.h>

#include <stm32l4_gpio.h>
#include "nucleo-l476rg.h"

#ifndef CONFIG_STM32L4_SPI1
#error spi1 required
#endif

#if defined(CONFIG_GENERICRADIO_CC1101)

struct cc1101_priv_s
{
  xcpt_t handler;
  FAR void *arg;
};

int cc1101_attach(FAR const struct cc1101_lower_s *lower, xcpt_t handler, FAR void *arg)
{
  struct cc1101_priv_s *priv = lower->privpointer;
  priv->handler = handler;
  priv->arg     = arg;
  return 0;
}

void cc1101_enable(FAR const struct cc1101_lower_s *lower, int state)
{
  //struct cc1101_priv_s *priv = lower->privpointer;
  if(state)
    {
      //stm32l4_gpiosetevent(GPIO_IRQ_CC1101, false, true, false, priv->handler, priv->arg);
    }
  else
    {
      //stm32l4_gpiosetevent(GPIO_IRQ_CC1101, false, false, false, NULL, NULL);
    }
}

struct cc1101_priv_s cc1101_priv;

const struct cc1101_lower_s cc1101_lower =
{
  cc1101_attach,
  cc1101_enable,
  &cc1101_priv
};
#endif

#if defined(CONFIG_GENERICRADIO_SI4463)

struct si4463_priv_s
{
  xcpt_t handler;
  FAR void *arg;
};

int si4463_attach(FAR const struct si4463_lower_s *lower, xcpt_t handler, FAR void *arg)
{
  struct si4463_priv_s *priv = lower->privpointer;
  priv->handler = handler;
  priv->arg     = arg;
  return 0;
}

void si4463_enable(FAR const struct si4463_lower_s *lower, int state)
{
  struct si4463_priv_s *priv = lower->privpointer;
  if(state)
    {
      stm32l4_gpiosetevent(GPIO_IRQ_SI4463, /*rising*/ false, /*falling*/ true, /*event*/ false, priv->handler, priv->arg);
    }
  else
    {
      stm32l4_gpiosetevent(GPIO_IRQ_SI4463, /*rising*/ false, /*falling*/ false, /*event*/ false, NULL, NULL);
    }
}

struct si4463_priv_s si4463_priv;

const struct si4463_lower_s si4463_lower =
{
  si4463_attach,
  si4463_enable,
  &si4463_priv
};
#endif

#if defined(CONFIG_GENERICRADIO_SX1231)

struct sx1231_priv_s
{
  xcpt_t handler;
  FAR void *arg;
};

int sx1231_attach(FAR const struct sx1231_lower_s *lower, xcpt_t handler, FAR void *arg)
{
  struct sx1231_priv_s *priv = lower->privpointer;
  priv->handler = handler;
  priv->arg     = arg;
  return 0;
}

void sx1231_enable(FAR const struct sx1231_lower_s *lower, int state)
{
  struct sx1231_priv_s *priv = lower->privpointer;
  if(state)
    {
      //stm32l4_gpiosetevent(GPIO_IRQ_SX1231, false, true, false, priv->handler, priv->arg);
    }
  else
    {
      //stm32l4_gpiosetevent(GPIO_IRQ_SX1231, false, false, false, NULL, NULL);
    }
}

struct sx1231_priv_s sx1231_priv;

const struct sx1231_lower_s sx1231_lower =
{
  sx1231_attach,
  sx1231_enable,
  &sx1231_priv
};
#endif

void stm32l4_genradioinitialize(void)
{
  FAR struct genradio_dev_s *radio;
  int ret;
  FAR struct spi_dev_s *spi1;

  /* Initialize SPI bus */

  spi1 = stm32l4_spibus_initialize(1);
  if (!spi1)
    {
      spierr("ERROR: FAILED to initialize SPI port 1\n");
    }

  /* Initialize radio devices */

#if defined(CONFIG_GENERICRADIO_CC1101)
  radio = A110LR0Nx_init(spi1, 0, &cc1101_lower);
  if(radio==NULL)
    {
      _err("Unable to initialize cc1101\n");
    }
  else
    {
      ret = genradio_register(radio, "/dev/gr0");
      if(ret)
        {
          _err("Failed to register cc1101 /dev/gr0\n");
        }
    }
#endif

#if defined(CONFIG_GENERICRADIO_SI4463)
  stm32l4_configgpio(GPIO_IRQ_SI4463);
  radio = RFM26_init(spi1, 1, SI4463_IO0, SI4463_IO1, &si4463_lower);
  if(radio==NULL)
    {
      _err("Unable to initialize si4463\n");
    }
  else
    {
      ret = genradio_register(radio, "/dev/gr1");
      if(ret)
        {
          _err("Failed to register si4463 /dev/gr1\n");
        }
    }
#endif

#if defined(CONFIG_GENERICRADIO_SX1231)
  radio = sx1231_init(spi1, 2, &sx1231_lower);
  if(radio==NULL)
    {
      _err("Unable to initialize sx1231\n");
    }
  else
    {
      ret = genradio_register(radio, "/dev/gr2");
      if(ret)
        {
          _err("Failed to register sx1231 /dev/gr2\n");
        }
    }
#endif
}
